import React, { Component } from "react";
import App from "./View/presenter";

class Container extends Component {
    //state 초기 값 세팅
    state = {
        isClock: false,
        isName: false,
        isTodo: false,
        isWeather: false,
        date: new Date()
    }

    _getTime = () => {
        const date = new Date();
        const hours = date.getHours();
        const minutes = date.getMinutes();
        const seconds = date.getSeconds();
    }

    render() {
        const { isClock } = this.state;
        const { isName } = this.state;
        const { isTodo } = this.state;
        const { isWeather } = this.state;
        const { date } = this.state;

        return <App {...this.props} {...this.state} />;
    }

    _isValidName = () => {
        this.setState({
            isName: true
        })
    };

    _isValidTodo = () => {
        this.setState({
            isTodo: true
        })
    };

    _isValidClock = () => {
        this.setState({
            isClock: true
        })
    };

    _isValidWeather = () => {
        this.setState({
            isWeather: true
        })
    };
        

}

export default Container;
