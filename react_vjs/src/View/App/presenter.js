import React, { Fragment } from 'react';
import { createGlobalStyle } from 'styled-components';
import Clock from 'components/clock';
import Weather from 'components/weather';
import Name from 'components/name';
import Background from 'components/background';
import Todo from 'components/todo';

const GlobalStyle = createGlobalStyle`
  * { padding: 0; margin: 0; box-sizing: border-box; }
  ul:empty,
  form:empty { display: none;}
  body { font: 14px/1.6 sans-serif; padding: 20px;}
  ul, li { list-style: none; }
  dfn { font-style: normal;}
  table { width: 100%; border-collapse: collapse;}  
`;

const App = props => {
  //const date = props.date;
  //console.log(this.state)
  return (
    <Fragment>
      <GlobalStyle />
      <Background />
      <Clock />
      <Weather />
      <Name />
      <Todo />
    </Fragment>
  );
};

export default App;
