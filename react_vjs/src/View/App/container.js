import React, { Component } from 'react';
import App from './presenter';

class Container extends Component {
  //state 초기 값 세팅
  state = {
    isName: false,
    isTodo: false,
    isWeather: false,
  };

  render() {
    const { isName } = this.state;
    const { isTodo } = this.state;
    const { isWeather } = this.state;

    return <App isName={isName} isTodo={isTodo} isWeather={isWeather} />;
  }

  _isValidName = () => {
    this.setState({
      isName: true,
    });
  };

  _isValidTodo = () => {
    this.setState({
      isTodo: true,
    });
  };

  _isValidWeather = () => {
    this.setState({
      isWeather: true,
    });
  };
}

export default Container;
