import React from 'react';
import styled from 'styled-components';

const Title = styled.h4`
  text-align: center;
  font-size: 30px;
`;

const Weather = props => {
  const { temp } = props;
  const { place } = props;
  const { weatherStatus } = props;

  return (
    <Title>
      {place}, {temp}°C, {weatherStatus}{' '}
    </Title>
  );
};

export default Weather;
