import React, { Component } from 'react';
import Weather from './presenter';

const COORDS = 'coords';
const API_KEY = '3f9404e51a676528522e910eefbc4158';

class Container extends Component {
  state: {
    temp: '',
    place: '',
    weatherStatus: '',
  };

  componentWillMount() {
    this._loadCoords();
  }

  render() {
    return <Weather {...this.state} />;
  }

  _errorCoords = () => {
    console.log('error call coords!');
  };

  _getWeather = (lat, long) => {
    fetch(
      `https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${long}&appid=${API_KEY}&units=metric`,
    )
      .then(function(response) {
        return response.json();
      })
      .then(
        function(json) {
          //console.log(json);
          this.setState({
            temp: json.main.temp,
            place: json.name,
            weatherStatus: json.weather[0].main,
          });
          //console.log(this.state);
        }.bind(this),
      );
  };

  _saveCoords = coordsObj => {
    localStorage.setItem(COORDS, JSON.stringify(coordsObj));
  };

  _successCoords = position => {
    //console.log(position);
    const latitude = position.coords.latitude;
    const longitude = position.coords.longitude;
    const coordsObj = {
      latitude,
      longitude,
    };
    this._saveCoords(coordsObj);
    this._getWeather(latitude, longitude);
  };

  _askForCoords = () => {
    navigator.geolocation.getCurrentPosition(
      this._successCoords,
      this._errorCoords,
    );
  };

  _loadCoords = () => {
    const loadedCoords = localStorage.getItem(COORDS);
    if (loadedCoords === null) {
      this._askForCoords();
    } else {
      const parsedCoords = JSON.parse(loadedCoords);
      this._getWeather(parsedCoords.latitude, parsedCoords.longitude);
    }
  };
}

export default Container;
