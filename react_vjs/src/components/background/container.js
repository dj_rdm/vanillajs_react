import React, { Component } from 'react';
import Background from './presenter';

class Container extends Component {
  state = {
    bgnum: '',
  };

  componentWillMount() {
    this._handlebg();
  }

  render() {
    //const { bgnum } = this.state;
    //console.log(this.state);
    return <Background {...this.state} />;
  }

  _handlebg = () => {
    const IMG_LENGTH = 4;
    const number = Math.floor(Math.random() * IMG_LENGTH) + 1;
    this.setState({
      bgnum: number,
    });
  };
}

export default Container;
