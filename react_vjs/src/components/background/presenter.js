import React from 'react';
import styled from 'styled-components';

const BgContainer = styled.div`
  position: fixed;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  z-index: -1;

  & img {
    width: 100%;
    height: 100%;
  }

  &:after {
    content: '';
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background-color: rgba(0, 0, 0, 0.4);
  }
`;

const Background = props => {
  const { bgnum } = props;
  //console.log(props);
  return (
    <BgContainer>
      <img src={require(`media/images/${bgnum}.jpg`)} alt="bg" />
    </BgContainer>
  );
};

export default Background;
