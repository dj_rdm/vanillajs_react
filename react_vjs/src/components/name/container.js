import React, { Component } from 'react';
import Name from './presenter';

const USER_LS = 'name';
const currentUser = localStorage.getItem(USER_LS);

class Container extends Component {
  state = {
    checkUser: false,
    value: '',
  };

  componentDidMount() {
    if (currentUser !== null) {
      this.setState({
        checkUser: true,
        value: currentUser,
      });
    }
  }

  render() {
    //const { checkUser } = this.state;

    //console.log(checkUser);
    return (
      <Name
        {...this.props}
        {...this.state}
        addUser={this._addUser}
        delUser={this._delUser}
        handleValue={this._handleValue}
      />
    );
  }

  _addUser = event => {
    const { value } = this.state;
    if (value !== '') {
      localStorage.setItem(USER_LS, value);
      this.setState({
        checkUser: true,
      });
      event.preventDefault();
    }
  };

  _delUser = () => {
    alert('사용자가 초기화 됩니다.');
    localStorage.removeItem(USER_LS);
    this.setState({
      checkUser: false,
    });
  };

  _handleValue = event => {
    const keyCode = event.which;
    const keyValue = event.target.value;
    if (keyCode === 13) {
      if (keyValue !== '') {
        this.setState({
          value: keyValue,
        });
      } else {
        alert('이름을 입력해주세요.');
        event.preventDefault();
      }
    }
  };
}

export default Container;
