import React, { Fragment } from 'react';
import styled from 'styled-components';

const Nameinput = styled.input`
  width: 100%;
  height: 40px;
  font-size: 16px;
  color: #000;
  padding: 0 10px;
  box-sizing: border-box;
`;

const GreetingMsg = styled.h3`
  font-size: 20px;
  margin-bottom: 20px;
  line-height: 1.6;
`;

const DelBtn = styled.button`
  vertical-align: middle;
  padding: 0 10px;
  font-size: 12px;
  margin-left: 10px;
`;

const Name = props => {
  //console.log(props);
  const { addUser } = props;
  const { checkUser } = props;
  const { handleValue } = props;
  const { delUser } = props;

  return (
    <Fragment>
      {checkUser === true ? (
        <GreetingMsg>
          환영합니다{' '}
          <strong>
            <q>{props.value}님</q>
          </strong>{' '}
          어서오세요
          <DelBtn type="button" onClick={delUser}>
            초기화
          </DelBtn>
        </GreetingMsg>
      ) : (
        <GreetingMsg>처음 오셨나요? </GreetingMsg>
      )}
      <form onSubmit={addUser}>
        {checkUser === false ? (
          <Nameinput
            type="search"
            placeholder="이름을 입력하세요"
            onKeyDown={handleValue}
          />
        ) : (
          ''
        )}
      </form>
    </Fragment>
  );
};

export default Name;
