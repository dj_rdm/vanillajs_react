import React, { Fragment } from 'react';
import styled from 'styled-components';

const Timetext = styled.h1`
  font-size: 40px;
  text-align: center;
`;

const Clock = props => {
  //console.log(props);
  return (
    <Fragment>
      <Timetext>{props.time}</Timetext>
    </Fragment>
  );
};

export default Clock;
