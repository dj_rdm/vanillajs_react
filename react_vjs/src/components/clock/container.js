import React, { Component } from 'react';
import Clock from './presenter';

//날짜 객체 생성
//const

class Container extends Component {
  //시간, 분, 초를 state로
  constructor(props) {
    super(props);
    this.state = { date: new Date() };
  }

  state = {
    //date: new Date(),
    time: '',
  };

  componentDidMount() {
    this.timerID = setInterval(() => this._tick(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  _tick() {
    this.setState({
      date: new Date(),
    });
  }

  render() {
    //console.log(this.state.date.toLocaleTimeString());
    return <Clock time={this.state.date.toLocaleTimeString()} />;
  }
}

export default Container;
