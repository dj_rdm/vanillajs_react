import React, { Fragment } from 'react';
import styled from 'styled-components';

const Todoinput = styled.input`
  font-size: 16px;
  height: 30px;
  width: 100%;
  padding: 0 10px;
`;
const TodoListContainer = styled.ul`
  margin-top: 20px;

  & li {
    font-size: 20px;
    font-weight: 700;
    color: rgba(255, 255, 255, 0.7);
  }
`;

const TodoNumber = styled.dfn`
  margin-right: 5px;
`;

const TodoDelBtn = styled.button`
  vertical-align: middle;
  padding: 0 10px;
  font-size: 14px;
  margin-left: 10px;
`;

const Todo = props => {
  const { addTodo } = props;
  const { handleTodo } = props;
  const { delTodo } = props;
  const { getTodos } = props;

  console.log(props);
  return (
    <Fragment>
      <form action="#none" onSubmit={addTodo}>
        <Todoinput
          type="search"
          placeholder="할 일을 입력하세요"
          onKeyDown={handleTodo}
        />
      </form>
      <TodoListContainer>
        {getTodos.map(toDoList => {
          return (
            <li key={toDoList.id} data-idx={toDoList.dfnId} id={toDoList.id}>
              <TodoNumber>{toDoList.dfnId}.</TodoNumber>
              {toDoList.text}
              <TodoDelBtn type="button" onClick={delTodo}>
                삭제하기
              </TodoDelBtn>
            </li>
          );
        })}
      </TodoListContainer>
    </Fragment>
  );
};

export default Todo;
