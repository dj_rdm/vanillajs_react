import React, { Component } from 'react';
import Todo from './presenter';

const TODO_LS = 'todos';
let toDos = [];

class Container extends Component {
  state = {
    checkTodo: false,
    value: '',
    toDos: {},
  };

  componentWillMount() {
    this._getTodos();
  }

  render() {
    //console.log(this.state);
    return (
      <Todo
        {...this.state}
        {...this.props}
        addTodo={this._addTodo}
        delTodo={this._delTodo}
        getTodos={toDos}
        handleTodo={this._handleTodo}
      />
    );
  }

  _getTodos() {
    const loadedToDos = localStorage.getItem(TODO_LS);
    if (loadedToDos !== null) {
      const parsedToDos = JSON.parse(loadedToDos);
      //console.log(parsedToDos);
      toDos = parsedToDos;
    }
  }

  _saveTodos() {
    localStorage.setItem(TODO_LS, JSON.stringify(toDos));
  }

  _addTodo = event => {
    const { value } = this.state;
    const newId = `${toDos.length + 1}`;
    const dfnId = `${toDos.length + 1}`;
    const toDoObj = {
      id: newId,
      dfnId: dfnId,
      text: value,
    };

    if (value !== '') {
      toDos.push(toDoObj);

      this.setState({
        checkTodo: true,
      });
      this._saveTodos();
      this._getTodos();
      event.preventDefault();
    }
  };

  _delTodo = event => {
    //alert('해당 할 일이 삭제됩니다.');
    //const { toDos } = this.state;
    const btn = event.target;
    const li = btn.parentNode;
    //const dfnId = btn.parentNode.dataset.idx;

    const cleanTodos = toDos.filter(toDo => toDo.dfnId !== li.dataset.idx);

    this.setState({
      toDos: cleanTodos,
    });

    toDos = cleanTodos;

    this._saveTodos();
  };

  _handleTodo = event => {
    const keyCode = event.which;
    const keyValue = event.target.value;
    if (keyCode === 13) {
      if (keyValue !== '') {
        this.setState({
          value: keyValue,
        });
        event.target.value = '';
      } else {
        alert('할 일을 입력해주세요.');
        event.preventDefault();
      }
    }
  };
}

export default Container;
